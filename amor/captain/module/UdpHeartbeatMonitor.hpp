#ifndef AMOR_CAPTAIN_MODULE_UDPHEARTBEATMONITOR_HPP
#define AMOR_CAPTAIN_MODULE_UDPHEARTBEATMONITOR_HPP

#include <iostream>
#include <map>
#include <string>

#include <boost/function.hpp>
#include <boost/asio.hpp>

#include <amor/captain/module/UdpHeartbeatData.hpp>
#include <amor/util/BoostTime/BoostTime.hpp>

namespace amor {
namespace captain {
namespace module {

/**
 * \brief Class allows receiving and analyzing of heart beats sent by modules and notifies other system components on death of a module.
 *
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup amor_captain_module
 */
class UdpHeartbeatMonitor
{
    public:

        /**
         * \brief Constructor.
         * \param port IP port the UdpHeartbeatMonitor listens on for incoming heart beats.
         */
        UdpHeartbeatMonitor( unsigned short port );

        /**
         * \brief Destructor.
         */
        virtual ~UdpHeartbeatMonitor();

        /**
         * \brief Starts operation of the heart beat monitor.
         */
        void start();

        /**
         * \brief Stops operation of the heart beat monitor.
         */
        void stop();

        /**
         * \brief Registers a callback function which is called if a module timed out.
         * \param callback Function object of callback function.
         *                 Callback function has to have one parameter "id" which is the ID of the module that timed out.
         *                 Example: boost::bind( &MyClass::myCallback, this, _1 )
         */
        void registerTimeoutCallback( boost::function < void( unsigned int id ) > callback );

        /**
         * \brief Returns a vector of all currently heart beat emitting modules.
         * \param Vector reference which is filled with module information inside this method.
         */
        void getEmittingModules( std::vector<UdpHeartbeatData>& modules );

    private:

        /**
         * \brief Starts an asynchrounous receive operation.
         */
        void receive();

        /**
         * \brief Handler method for asynchrounous receive operation.
         */
        void handleReceive( unsigned char* buffer, const boost::system::error_code& error, std::size_t bytesReceived );

        /**
         * \brief Loop that checks every heart beat sending module for a timeout at fixed operation frequency.
         * Is executed in threadCheckTimeout_ thread.
         */
        void checkTimeoutLoop();

    private:

        /// IO service.
        boost::asio::io_service ioService_;
        
        /// Thread for IO service.
        boost::thread* threadIoService_;

        /// UDP socket.
        boost::asio::ip::udp::socket socket_;
        
        /// UDP endpoint.
        boost::asio::ip::udp::endpoint remoteEndpoint_;

        /// Timeout check thread interruption hint.
        bool runThreadCheckTimeout_;
        
        /// Thread for timeout check.
        boost::thread* threadCheckTimeout_;
        
        /// Mutex to protect idDataMap_ member.
        boost::mutex mtxIdDataMapAccess_;

        /// Map storing data packets and associated ids.
        std::map< int, UdpHeartbeatData > idDataMap_;

        /// Callback registered hint.
        bool callbackRegistered_;
        
        /// Callback function for timeout.
        boost::function < void( unsigned int id )> callback_;
};

} // namespace module
} // namespace captain
} // namespace amor

#endif /* AMOR_CAPTAIN_MODULE_UDPHEARTBEATMONITOR_HPP */


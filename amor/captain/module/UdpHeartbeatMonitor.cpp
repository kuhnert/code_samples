#include "UdpHeartbeatMonitor.hpp"

using namespace amor::captain::module;

UdpHeartbeatMonitor::UdpHeartbeatMonitor( unsigned short port ) :
    threadIoService_( 0 ),
    socket_( ioService_, boost::asio::ip::udp::endpoint( boost::asio::ip::udp::v4(), port ) ),
    runThreadCheckTimeout_( false ),
    threadCheckTimeout_( 0 ),
    callbackRegistered_( false )
{
}

UdpHeartbeatMonitor::~UdpHeartbeatMonitor()
{
    stop();
    socket_.close();
}

void UdpHeartbeatMonitor::start()
{
    receive();
    runThreadCheckTimeout_ = true;
    threadCheckTimeout_ = new boost::thread( boost::bind( &UdpHeartbeatMonitor::checkTimeoutLoop, this ) );
    ioService_.reset();
    threadIoService_ = new boost::thread( boost::bind( &boost::asio::io_service::run, &ioService_ ) );
}

void UdpHeartbeatMonitor::stop()
{
    // Cleanup.
    runThreadCheckTimeout_ = false;
    if ( threadCheckTimeout_ )
    {
        threadCheckTimeout_->join();
        delete threadCheckTimeout_;
        threadCheckTimeout_ = 0;
    }

    ioService_.stop();
    if ( threadIoService_ )
    {
        threadIoService_->join();
        delete threadIoService_;
        threadIoService_ = 0;
    }
}

void UdpHeartbeatMonitor::registerTimeoutCallback( boost::function < void( unsigned int id ) > callback )
{
    callback_ = callback;
    callbackRegistered_ = true;
}

void UdpHeartbeatMonitor::getEmittingModules( std::vector<UdpHeartbeatData>& modules )
{
    boost::mutex::scoped_lock lock( mtxIdDataMapAccess_ );
    
    // Collect emmiting modules.
    for( std::map<int,UdpHeartbeatData>::iterator it = idDataMap_.begin(); it != idDataMap_.end(); ++it )
    {
        modules.push_back( it->second );
    }
}

void UdpHeartbeatMonitor::receive()
{
    size_t size = sizeof( UdpHeartbeatData );
    unsigned char* buffer = new unsigned char[ size ];

    // Start async receive operation on socket.
    socket_.async_receive_from(
        boost::asio::buffer( buffer, size ),
        remoteEndpoint_,
        boost::bind(
            &UdpHeartbeatMonitor::handleReceive,
            this,
            buffer,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred ) );
}

void UdpHeartbeatMonitor::handleReceive( unsigned char* buffer, const boost::system::error_code& error, std::size_t /* bytesReceived */ )
{
    if ( !error || error == boost::asio::error::message_size )
    {
        UdpHeartbeatData* data = reinterpret_cast<UdpHeartbeatData*>( buffer );

        // Set local receive timestamp of received data.
        // Attention: This overwrites the received remote data timestamp.
        // If the remote data timestamp is also wanted then it has to be saved locally beforehands.
        // We do that so we are independent from different clocks on different machines.
        data->timestamp = amor::util::BoostTime::timestamp();

        // Copy new data into id-data map for next cycle. Protected by mutex.
        {
            boost::mutex::scoped_lock lock( mtxIdDataMapAccess_ );

            // Determine if we have a beat or a freeze message.
            if ( data->type == UdpHeartbeatData::PACKETTYPE_BEAT )
            {
                // If it is a beat message, update the associated data set.
                idDataMap_[data->id] = *data;
            }
            else
            {
                // If it is a freeze message, remove the associated data set from id-data map.
                std::map< int, UdpHeartbeatData >::iterator it = idDataMap_.find( data->id );
                idDataMap_.erase( it );                
            }
        }

        // Start next asynchronous receive operation.
        receive();
    }

    delete[] buffer;
}

void UdpHeartbeatMonitor::checkTimeoutLoop()
{
    while ( runThreadCheckTimeout_ )
    {
        double tCurrent = amor::util::BoostTime::timestamp();

        // Use id-data map for timeout checking. Protected by mutex.
        {
            boost::mutex::scoped_lock lock( mtxIdDataMapAccess_ );

            for( std::map<int,UdpHeartbeatData>::iterator it = idDataMap_.begin(); it != idDataMap_.end(); ++it )
            {
                // Calculate timestamp difference between current timestamp and timestamp stored in the data map [in seconds].
                double tDiff = tCurrent - it->second.timestamp;

                // Check if difference is bigger than timeout threshold. Threshold is also communicated in the UdpHeartbeatData dataset.
                if ( tDiff > it->second.timeout )
                {
                    // Handle timeout by calling timeout callback.
                    if ( callbackRegistered_ )
                    {
                        callback_( it->second.id );
                    }
                    else
                    {
                        std::cerr << "WARNING in UdpHeartbeatMonitor::checkTimeoutLoop() No timeout callback method registered." << std::endl;
                    }

                    // Remove entry because the module is dead and do not want to check it in the future.
                    idDataMap_.erase( it );
                }
            }
        }

        usleep( 100000 );
    }
}


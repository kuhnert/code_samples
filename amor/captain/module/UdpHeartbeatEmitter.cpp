#include "UdpHeartbeatEmitter.hpp"

using namespace amor::captain::module;

UdpHeartbeatEmitter::UdpHeartbeatEmitter( std::string address, unsigned short port, unsigned char id, double timeout ) :
    socket_( ioService_, boost::asio::ip::udp::v4() )
{
    boost::asio::ip::address ip;
    ip.from_string( address );
    receiverEndpoint_ = boost::asio::ip::udp::endpoint( ip, port );

    data_.counter = 0;
    data_.id = id;
    data_.timestamp = amor::util::BoostTime::timestamp();
    data_.timeout = timeout;
}

UdpHeartbeatEmitter::~UdpHeartbeatEmitter()
{
    socket_.close();
}

void UdpHeartbeatEmitter::beat()
{
    data_.type = static_cast<unsigned char>( UdpHeartbeatData::PACKETTYPE_BEAT );
    send();
}

void UdpHeartbeatEmitter::freeze()
{
    data_.type = static_cast<unsigned char>( UdpHeartbeatData::PACKETTYPE_FREEZE );
    send();
}

void UdpHeartbeatEmitter::send()
{
    data_.counter++;
    data_.timestamp = amor::util::BoostTime::timestamp();

    size_t size = sizeof( UdpHeartbeatData );
    unsigned char* buffer = reinterpret_cast<unsigned char*>( &data_ );
    socket_.send_to( boost::asio::buffer( buffer, size ), receiverEndpoint_ );
}


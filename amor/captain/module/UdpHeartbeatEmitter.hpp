#ifndef AMOR_CAPTAIN_MODULE_UDPHEARTBEATEMITTER_HPP_
#define AMOR_CAPTAIN_MODULE_UDPHEARTBEATEMITTER_HPP_

#include <iostream>
#include <string>

#include <boost/asio.hpp>

#include <amor/captain/module/UdpHeartbeatData.hpp>
#include <amor/util/BoostTime/BoostTime.hpp>

namespace amor {
namespace captain {
namespace module {

/**
 * \brief UdpHeartbeatEmitter Class allows emission of module heartbeats communicated via UDP sockets.
 *
 * This class can be used to communicate that a module is still running by sending heartbeats via UDP sockets.
 * The class UdpHeartbeatMonitor receives and analyzes these heartbeats and notifies other system components on death of a module.
 *
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup amor_captain_module
 */
class UdpHeartbeatEmitter
{
    public:

        /**
         * \brief Constructor.
         *
         * \param address   IP address of the UdpHeartbeatMonitor.
         * \param port      IP port of the UdpHeartbeatMonitor.
         * \param id        ID of the module which is associated with this heartbeat. Attention: This has to be unique for each module (system-wide).
         * \param timeout   Timeout period of the module which is associated with this heartbeat [in seconds].
         *                  If this period elapses without a new heartbeat emission it is assumed that the module is dead.
         */
        UdpHeartbeatEmitter( std::string address, unsigned short port, unsigned char id, double timeout );

        /**
         * \brief Destructor.
         */
        virtual ~UdpHeartbeatEmitter();

        /**
         * \brief Emits a heartbeat message.
         * Use this method to continously indicate that a module is still alive.
         */
        void beat();

        /**
         * \brief Emits a heart freeze message.
         * Use this method to indicate that a module will not send heartbeats anymore. The monitor will therefore not produce a timeout for the module.
         * The state remains like this until a new beat is sent.
         */
        void freeze();

    private:

        /**
         * Sends the current message by updating and sending the current UdpHeartbeatData object to the previously specified UdpHeartbeatMonitor via UDP.
         */
        void send();

    private:

        /// IO service.
        boost::asio::io_service ioService_;
        
        /// UDP socket.
        boost::asio::ip::udp::socket socket_;
        
        /// UDP endpoint.
        boost::asio::ip::udp::endpoint receiverEndpoint_;
        
        /// Outgoing data.
        UdpHeartbeatData data_;
};

} // namespace module
} // namespace captain
} // namespace amor

#endif /* AMOR_CAPTAIN_MODULE_UDPHEARTBEATEMITTER_HPP_ */


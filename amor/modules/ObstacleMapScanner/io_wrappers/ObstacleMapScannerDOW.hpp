#ifndef AMOR_MODULES_OBSTACLEMAPSCANNERDOW_HPP_
#define AMOR_MODULES_OBSTACLEMAPSCANNERDOW_HPP_

#include <amor/captain/module/exception/ModuleException.hpp>
#include <amor/clients/ObstacleMapScannerClient/ObstacleMapScannerData.hpp>
#include <amor/util/BoostTime/BoostTime.hpp>

namespace amor {
namespace modules {

/**
 * \brief Abstract data output wrapper of module ObstacleMapScanner.
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup ObstacleMapScanner
 */
class ObstacleMapScannerDOW
{
    public:
        /**
         * \brief Method initializes the wrapper.
         * \throw amor::captain::module::exception::ModuleException If initialization errors occur.
         */
        virtual void init() = 0;

        /**
         * \brief Method sends scanline synchronously (method blocks until completion).
         * \param scanline Struct storing one scanline.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void syncWriteScanline( ObstacleMapScannerData& scanline ) = 0;
                
        /**
         * \brief Indicates that data should be written. Can be ignored by a concrete implementation.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void writeAllData() = 0;
};

} // namespace modules
} // namespace amor

#endif // AMOR_MODULES_OBSTACLEMAPSCANNERDOW_HPP_

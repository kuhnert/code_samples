#ifndef AMOR_MODULES_OBSTACLEMAPSCANNERDOW_ROS_HPP_
#define AMOR_MODULES_OBSTACLEMAPSCANNERDOW_ROS_HPP_

#include <ros/ros.h>

#include <amor/types/Vector3.hpp>
#include <amor/util/BoostTime/BoostTime.hpp>
#include <amor/util/Rotation/Rotation.hpp>

#include <amor/modules/ObstacleMapScanner/io_wrappers/ObstacleMapScannerDOW.hpp>

#include <ezls_msgs/CartesianScanline.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/LaserScan.h>

namespace amor {
namespace modules {

/**
 * \brief Concrete data output wrapper of module ObstacleMapScanner using ROS Publisher/Subscriber mechanism.
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup ObstacleMapScanner
 */
class ObstacleMapScannerDOW_ROS : public ObstacleMapScannerDOW
{
    private:

        /// ROS publisher for output data.
        ros::Publisher pubData_;

        /// ROS publisher for LaserScan messages.
        ros::Publisher pubScan_;

        /// Topic name for data publisher.
        std::string topicData_;

        /// Topic name for data publisher.
        std::string topicScan_;

        /// Publisher queue sizes.
        int queueSize_;

    public:

        /**
         * \brief Constructor.
         */
        ObstacleMapScannerDOW_ROS( std::string topicData, std::string topicScan, int queueSize );

        /**
         * \brief Destructor.
         */
        virtual ~ObstacleMapScannerDOW_ROS();

        /**
         * \brief Method initializes the Wrapper.
         * \throw amor::captain::module::exception::ModuleException If initialization errors occur.
         */
        virtual void init ();

        /**
         * \brief Method sends scanline synchronously (method blocks until completion).
         * \param scanline Struct storing one scanline
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void syncWriteScanline( ObstacleMapScannerData& scanline );
        
        /**
         * \brief Indicates that data should be written. Can be ignored by a concrete implementation.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void writeAllData();
};

} // namespace modules
} // namespace amor

#endif /* AMOR_MODULES_OBSTACLEMAPSCANNERDOW_ROS_HPP_ */

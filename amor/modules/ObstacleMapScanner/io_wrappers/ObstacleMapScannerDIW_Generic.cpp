#include "ObstacleMapScannerDIW_Generic.hpp"

using namespace amor::types;
using namespace amor::modules;

ObstacleMapScannerDIW_Generic::ObstacleMapScannerDIW_Generic( Odometry3DClient* odoClient, PathPlanningLocalClient* pplClient ) :
    odoClient_( odoClient ),
    pplClient_( pplClient )
{
}

ObstacleMapScannerDIW_Generic::~ObstacleMapScannerDIW_Generic()
{
}

void ObstacleMapScannerDIW_Generic::init()
{
    try
    {
        odoClient_->init();
        pplClient_->init();
    }
    catch( amor::comm::ipc::exception::IpcException& ex )
    {
        std::cerr << ex.what() << std::endl;
    }
}

amor::types::PoseQ3 ObstacleMapScannerDIW_Generic::getCurrentVehiclePose()
{
    Odometry3DData data = odoClient_->getData();
    return data.pose3Filter;
}

void ObstacleMapScannerDIW_Generic::registerCallbackAsyncObstacleMap(boost::function<void( amor::types::MultiLayerHeightField*, amor::types::PoseQ3& )> callback )
{
    pplClient_->registerCallbackMap( callback );
}

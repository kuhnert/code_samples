#include <ezls_amor_modules/ObstacleMapScanner/DOW/ObstacleMapScannerDOW_ROS.hpp>

using namespace amor::modules;

ObstacleMapScannerDOW_ROS::ObstacleMapScannerDOW_ROS(std::string topicData, std::string topicScan, int queueSize ) :
    topicData_( topicData ),
    topicScan_( topicScan ),
    queueSize_ ( queueSize )
{
}

ObstacleMapScannerDOW_ROS::~ObstacleMapScannerDOW_ROS()
{
}

void ObstacleMapScannerDOW_ROS::init()
{
    ros::NodeHandle nh;
    pubData_ = nh.advertise<ezls_msgs::CartesianScanline>( topicData_.c_str(), queueSize_ );
    pubScan_ = nh.advertise<sensor_msgs::LaserScan>( topicScan_.c_str(), queueSize_ );
}

void ObstacleMapScannerDOW_ROS::syncWriteScanline( ObstacleMapScannerData& scanline )
{
    // Init LaserScan message.
    sensor_msgs::LaserScan scan;
    scan.header.stamp = ros::Time::now();
    scan.header.frame_id = "base_oms_laser";
    scan.angle_min = scanline.minAngle;
    scan.angle_max = scanline.maxAngle;
    scan.angle_increment = scanline.resAngle;
    scan.range_min = 0.0;
    scan.range_max = 32.0;

    // Init CartesianScanline message.
    ezls_msgs::CartesianScanline csl;
    csl.header.stamp = ros::Time::now();
    csl.header.frame_id = "amor";

    double angle_offset = 0.0;
    double current_angle = scanline.minAngle + angle_offset;
    int i=0;
    while ( current_angle <= scanline.maxAngle + angle_offset )
    {
        // Add ranges to LaserScan.
        scan.ranges.push_back( scanline.distances[i] );

        // Add cartesian points to CartesianScanline.
        geometry_msgs::Point32 p;
        p.x = -sin( current_angle ) * scanline.distances[i];
        p.y = +cos( current_angle ) * scanline.distances[i];
        p.z = csl.distanceOverGround;
        csl.points.push_back( p );

        current_angle += scanline.resAngle;
        i++;
    }

    pubScan_.publish( scan );
    pubData_.publish( csl );
}

void ObstacleMapScannerDOW_ROS::writeAllData()
{
    // Do nothing.
}

#ifndef AMOR_MODULES_OBSTACLEMAPSCANNERDIW_GENERIC_HPP_
#define AMOR_MODULES_OBSTACLEMAPSCANNERDIW_GENERIC_HPP_

#include <fstream>
#include <iomanip>

#include <amor/comm/ipc/exception/IpcException.hpp>
#include <amor/clients/Odometry3DClient/Odometry3DClient.hpp>
#include <amor/clients/PathPlanningLocalClient/PathPlanningLocalClient.hpp>
#include <amor/modules/ObstacleMapScanner/io_wrappers/ObstacleMapScannerDIW.hpp>

namespace amor {
namespace modules {

/**
 * \brief Concrete data input wrapper of module ObstacleMapScanner getting data with abstract module clients.
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup ObstacleMapScanner
 */
class ObstacleMapScannerDIW_Generic : public ObstacleMapScannerDIW
{
    private:
        
        /// Client for getting odometry localization information.
        Odometry3DClient* odoClient_;
        
        /// Client for getting local path planning information.
        PathPlanningLocalClient* pplClient_;
        
    public:
        /**
         * \brief Constructor.
         * \param odoClient Client for getting odometry localization information.        
         * \param pplClient Client for getting local path planning information.
         */
        ObstacleMapScannerDIW_Generic( Odometry3DClient* odoClient, PathPlanningLocalClient* pplClient );

        /**
         * \brief Destructor.
         */
        ~ObstacleMapScannerDIW_Generic();

        /**
         * \brief Method initializes the Wrapper.
         * \throw amor::captain::module::exception::ModuleException If initialization errors occur.
         */
        virtual void init();

        /**
         * \brief Method fetches current vehicle pose from odometry.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         * \return Current vehicle pose.
         */
        virtual amor::types::PoseQ3 getCurrentVehiclePose();

        /**
         * \brief Registers callback function which is called after a new obstacle map is asynchronously received.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void registerCallbackAsyncObstacleMap( boost::function <void( amor::types::MultiLayerHeightField* map, amor::types::PoseQ3& pose )> callback );
};

} // namespace modules
} // namespace amor


#endif /* AMOR_MODULES_OBSTACLEMAPSCANNERDIW_GENERIC_HPP_ */


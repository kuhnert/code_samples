#ifndef AMOR_MODULES_OBSTACLEMAPSCANNERDOW_SHMEM_HPP_
#define AMOR_MODULES_OBSTACLEMAPSCANNERDOW_SHMEM_HPP_

#include <amor/clients/ObstacleMapScannerClient/ObstacleMapScanner_ShMemInfo.hpp>
#include <amor/comm/ipc/SimpleSharedMemoryWriter.hpp>
#include <amor/modules/ObstacleMapScanner/io_wrappers/ObstacleMapScannerDOW.hpp>

namespace amor {
namespace modules {

/**
 * \brief Concrete data output wrapper of module ObstacleMapScanner using boost ipc shared memory communication.
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup ObstacleMapScanner
 */
class ObstacleMapScannerDOW_ShMem : public ObstacleMapScannerDOW
{
    public:

        /**
         * \brief Constructor.
         */
        ObstacleMapScannerDOW_ShMem();

        /**
         * \brief Destructor.
         */
        ~ObstacleMapScannerDOW_ShMem();

        /**
         * \brief Method initializes the Wrapper.
         * \throw amor::captain::module::exception::ModuleException If initialization errors occur.
         */
        virtual void init();

        /**
         * \brief Method sends scanline synchronously (method blocks until completion).
         * \param scanline Struct storing one scanline
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void syncWriteScanline( ObstacleMapScannerData& scanline );
        
        /**
         * \brief Indicates that data should be written. Can be ignored by a concrete implementation if not needed.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void writeAllData();

    private:

        /// Data structures which stores the latest output data.
        ObstacleMapScannerData outputData_;
 
        /// SimpleSharedMemoryWriter for ObstacleMapScannerData object.
        amor::comm::ipc::SimpleSharedMemoryWriter<ObstacleMapScannerData>* shMemWriter_;
};

} // namespace modules
} // namespace amor

#endif /* AMOR_MODULES_OBSTACLEMAPSCANNERDOW_SHMEM_HPP_ */

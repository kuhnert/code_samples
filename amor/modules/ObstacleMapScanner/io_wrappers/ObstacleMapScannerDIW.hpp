#ifndef AMOR_MODULES_OBSTACLEMAPSCANNERDIW_HPP_
#define AMOR_MODULES_OBSTACLEMAPSCANNERDIW_HPP_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

#include <amor/captain/module/exception/ModuleException.hpp>
#include <amor/types/PoseQ3.hpp>
#include <amor/types/MultiLayerHeightField.hpp>

namespace amor {
namespace modules {

/**
 * \brief Abstract data input wrapper of module ObstacleMapScanner.
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup ObstacleMapScanner
 */
class ObstacleMapScannerDIW
{
    public:
        
        /**
         * \brief Method initializes the Wrapper.
         * \throw amor::captain::module::exception::ModuleException If initialization errors occur.
         */
        virtual void init () = 0;

        /**
         * \brief Method fetches current vehicle pose from odometry.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         * \return Current vehicle pose.
         */
        virtual amor::types::PoseQ3 getCurrentVehiclePose() = 0;

        /**
         * \brief Registers callback function which is called after a new obstacle map is asynchronously received.
         * \throw amor::captain::module::exception::CommunicationException on communication error.
         */
        virtual void registerCallbackAsyncObstacleMap( boost::function <void( amor::types::MultiLayerHeightField* map, amor::types::PoseQ3& pose )> callback ) = 0;
};

} // namespace modules
} // namespace amor

#endif /* AMOR_MODULES_OBSTACLEMAPSCANNERDIW_HPP_ */


#include "ObstacleMapScannerDOW_ShMem.hpp"

using namespace amor::modules;

ObstacleMapScannerDOW_ShMem::ObstacleMapScannerDOW_ShMem() :
    shMemWriter_( 0 )
{
}

ObstacleMapScannerDOW_ShMem::~ObstacleMapScannerDOW_ShMem()
{
    if ( shMemWriter_ ) { delete shMemWriter_; shMemWriter_ = 0; }
}

void ObstacleMapScannerDOW_ShMem::init()
{
    try
    {
        if ( !shMemWriter_ )
        {
            shMemWriter_ = new amor::comm::ipc::SimpleSharedMemoryWriter<ObstacleMapScannerData>( ObstacleMapScanner_ShMemInfo, "ObstacleMapScannerDOW_ShMem", amor::comm::ipc::NON_NOTIFYING_MODE );
        }
    }
    catch ( amor::comm::ipc::exception::IpcException & ex )
    {
        throw amor::captain::module::exception::ModuleException( ex.what() );
    }
}

void ObstacleMapScannerDOW_ShMem::syncWriteScanline( ObstacleMapScannerData& scanline )
{
    shMemWriter_->write( &scanline );
}

void ObstacleMapScannerDOW_ShMem::writeAllData()
{
    // Do nothing.
}

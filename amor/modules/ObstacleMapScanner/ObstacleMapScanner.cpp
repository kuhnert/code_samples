#include "ObstacleMapScanner.hpp"

using namespace amor::captain::core;
using namespace amor::modules;

ObstacleMapScanner::ObstacleMapScanner( ObstacleMapScannerDIW * diw,
                                        ObstacleMapScannerDOW * dow,
                                        amor::captain::module::AbstractEventInputWrapper * eiw,
                                        amor::captain::module::AbstractEventOutputWrapper * eow,
                                        amor::captain::module::AbstractCommandInputWrapper<ObstacleMapScannerCmd> * ciw,
                                        amor::captain::module::AbstractCommandOutputWrapper<ObstacleMapScannerCmd> * cow,
                                        std::string rootMissionXmlPath,
                                        amor::types::Vector3 vehicleSize,
                                        double minAngle,
                                        double maxAngle,
                                        double resAngle,
                                        double scanFrequency,
                                        bool vis,
                                        bool log ) :        
    diw_( diw ),
    dow_( dow ),
    eiw_( eiw ),
    eow_( eow ),
    ciw_( ciw ),
    cow_( cow ),
    log_( log),
    state_( ObstacleMapScanner::INVALID ),
    heart_( "192.168.1.2", 22222, 7, 1.0 ),
    map_( 0 ),
    minAngle_( minAngle ),
    maxAngle_( maxAngle ),
    resAngle_( resAngle ),
    scanFrequency_( scanFrequency ),
    scanPeriod_( 1.0 / scanFrequency_ ),
    threadScanLoop_( 0 )
{    
    // Initialize data output wrapper.
    dow_->init();

    // Initialize event output wrapper.
    eow_->event_access.readEventListFromFile( rootMissionXmlPath );
    eow_->init();

    // Initialize command input wrappers.
    ciw_->init();
    ciw_->registerCmdCallback( boost::bind( &ObstacleMapScanner::processCommand, this, _1 ) );

    state_ = ObstacleMapScanner::IDLE;
    sendEvent( "OBSTACLEMAPSCANNER_STATE_IDLE" );
}

ObstacleMapScanner::~ObstacleMapScanner()
{
    stop();
    reset();
}

void ObstacleMapScanner::init()
{
    if ( state_ == ObstacleMapScanner::IDLE )
    {        
        // Initialize data input wrapper.
        diw_->init();

        // Register callback method for async waypoint vector processing.
        diw_->registerCallbackAsyncObstacleMap( boost::bind( &ObstacleMapScanner::processObstacleMap, this, _1, _2 ) );
        
        state_ = ObstacleMapScanner::INIT;
        sendEvent( "OBSTACLEMAPSCANNER_STATE_INIT" );
    }
    else
    {
        std::cout << "WARNING in ObstacleMapScanner::init() Could not init because module is not in IDLE state." << std::endl;
    }
}

void ObstacleMapScanner::start()
{
    if ( ( state_ == ObstacleMapScanner::INIT ) ||
         ( state_ == ObstacleMapScanner::STOP ) ||
         ( state_ == ObstacleMapScanner::RESET ) )
    {
        // Start track path local loop thread.
        runScan_ = true;
        threadScanLoop_ = new boost::thread( boost::bind( &ObstacleMapScanner::scanLoop, this ) );
        
        state_ = ObstacleMapScanner::START;
        sendEvent( "OBSTACLEMAPSCANNER_STATE_START" );
    }
    else
    {
        std::cout << "WARNING in ObstacleMapScanner::start() Could not start because module is not in INIT/STOP/RESET state." << std::endl;
    }
}

void ObstacleMapScanner::stop()
{
    if ( state_ == ObstacleMapScanner::START )
    {
        // Check if module is already stopped.
        if ( runScan_ )
        {
            // Stop, join and delete processing thread.
            runScan_ = false;
            if (threadScanLoop_)
            {
                threadScanLoop_->join();
                delete threadScanLoop_;
                threadScanLoop_ = 0;
            }
        }
        else
        {
            std::cout << "ObstacleMapScanner::stop() Module is already stopped. stop() not possible." << std::endl;
        }

        // Emit heart freeze to the HeartbeatMonitor to tell that we are not sending from now on.
        heart_.freeze();

        state_ = ObstacleMapScanner::STOP;
        sendEvent( "OBSTACLEMAPSCANNER_STATE_STOP" );
    }
    else
    {
        std::cout << "WARNING in ObstacleMapScanner::stop() Could not stop because module is not in START state." << std::endl;
    }
}

void ObstacleMapScanner::reset()
{    
    if ( state_ == ObstacleMapScanner::STOP )
    {
        state_ = ObstacleMapScanner::RESET;
        sendEvent( "OBSTACLEMAPSCANNER_STATE_RESET" );
    }
    else
    {
        std::cout << "WARNING in ObstacleMapScanner::reset() Could not stop because module is not in STOP state." << std::endl;
    }
}

void ObstacleMapScanner::processCommand( boost::shared_ptr<ObstacleMapScannerCmd> cmd_ptr )
{
    if ( state_ != ObstacleMapScanner::INVALID )
    {
        std::cout << std::fixed
                  << amor::util::BoostTime::timestamp()
                  << " ObstacleMapScanner: Processing incoming command ... "
                  << cmd_ptr->getStringDescriptor() << std::endl;

        switch ( cmd_ptr->command )
        {
            case ObstacleMapScannerCmd::INIT:
                init();
                break;
            case ObstacleMapScannerCmd::START:
                start();
                break;
            case ObstacleMapScannerCmd::STOP:
                stop();
                break;
            case ObstacleMapScannerCmd::RESET:
                reset();
                break;
            default:
                break;
        }
    }
}

bool ObstacleMapScanner::sendEvent( std::string name,
                                    amor::captain::core::Event::PRIORITY priority,
                                    bool isAddressed,
                                    amor::captain::core::GROUP_ID addressee )
{
    if ( state_ != ObstacleMapScanner::INVALID )
    {
        // Get event ID by name.
        EVENT_ID id = eow_->lookupEventId( name );

        // Throw exception because an invalid event name was used.
        if ( id == 0 )
        {
            std::cerr << "ERROR in ObstacleMapScanner::sendEvent(): Invalid event name requested. " << name << std::endl;
            throw;
        }

        // Send event.
        try
        {
            amor::captain::core::Event event;
            event.id = id;
            event.priority = priority;
            event.is_addressed = isAddressed;
            event.addressee = addressee;

            eow_->sendEvent( event );
        }
        catch( amor::captain::module::exception::CommunicationException& ex )
        {
            std::cerr << "ERROR in ObstacleMapScanner::sendEvent(): " << ex.what() << std::endl;
            return false;
        }

        return true;
    }

    return false;
}

void ObstacleMapScanner::processObstacleMap( amor::types::MultiLayerHeightField* map, amor::types::PoseQ3& pose )
{    
    boost::mutex::scoped_lock mapUpdateLock( mtxMapUpdate_ );

    std::cout << "Received obstacle map of size " << map->size().x() << "x" << map->size().y() << "." << std::endl;

    if ( map_ ) { delete map_; map_ = 0; }
    map_ = map;
    poseMap_ = pose;
}

void ObstacleMapScanner::scanLoop()
{
    amor::packages::geometry::ScanlineProjector<double> slp;

    unsigned int numBeams = static_cast<unsigned int>( ( maxAngle_ - minAngle_ ) / resAngle_ ) + 1;

    amor::types::PoseQ3 poseqReference;
    amor::types::PoseQ3 poseqCurrent;
    amor::types::PoseQ3 poseqProjected;
    amor::types::Pose3 poseCurrent;
    amor::types::Pose3 poseTemp;

    // Output data structure that holds one scanline.
    amor::modules::ObstacleMapScannerData scanline;
    scanline.timestamp = -1.0;
    scanline.numBeams = numBeams;
    scanline.minAngle = minAngle_;
    scanline.maxAngle = maxAngle_;
    scanline.resAngle = resAngle_;
    for ( unsigned int i=0; i<amor::modules::OMS_MAX_NUMBER_OF_BEAMS; i++ ) { scanline.distances[i] = -1.0; }

    int counter = 0;
    
    while( runScan_ )
    {        
        double tStart = amor::util::BoostTime::timestamp();

        if ( map_ )
        { // Mutex map update begin.
            
            boost::mutex::scoped_lock mapUpdateLock( mtxMapUpdate_ );

            // Get current reference pose.
            poseqReference = poseMap_;

            // Update current vehicle pose.
            poseqCurrent = diw_->getCurrentVehiclePose();

            // Project current vehicle pose to local scene coordinate system.
            poseqProjected = slp.projectPose( poseqCurrent, poseqReference );

            // Get yaw angle of vehicle from current quaternion.
            double vehiclePitch = 0.0, vehicleRoll = 0.0, vehicleYaw = 0.0;
            BoostQuaternion bq = poseqProjected.orientation.toBoostQuaternion();
            amor::util::Rotation::Quaternion2EulerXYZ( bq, vehiclePitch, vehicleRoll, vehicleYaw );

            poseCurrent.x( poseqProjected.position.x() );
            poseCurrent.y( poseqProjected.position.y() );
            poseCurrent.z( poseqProjected.position.z() );
            poseCurrent.pitch( 0.0 );
            poseCurrent.roll( 0.0 );
            poseCurrent.yaw( vehicleYaw );

            // Move scanner origin to front of vehicle.
            amor::types::Vector3 vHdg( -sin( vehicleYaw ), cos( vehicleYaw ), 0.0 );
            vHdg *= vehicleSize_.y() / 2.0;
            poseTemp = poseCurrent;
            poseTemp.position_ref() = poseCurrent.position() + vHdg;

            poseTemp = poseCurrent;

            scanline.timestamp = amor::util::BoostTime::timestamp();

            // Do ray casting with bresenham line algorithm.
            double angle = minAngle_;
            for ( unsigned int beam=0; beam<numBeams; beam++ )
            {
                double d = castRay( poseTemp, map_, angle );
                scanline.distances[beam] = d;
                angle += resAngle_;
            }

            counter++;

        } // Mutex map update end.
                    
        // Send scanline with DOW.
        dow_->syncWriteScanline( scanline );

        // Emit heartbeat to the HeartbeatMonitor that we are still alive.
        heart_.beat();
        
        double tEnd = amor::util::BoostTime::timestamp();
        double tElapsed = tEnd - tStart;
        double tDiff = scanPeriod_ - tElapsed;

        // Sleep to realize a constant fetching interval.
        if ( tDiff > 0.0 ) { amor::util::BoostTime::sleep( tDiff ); }
        else { std::cout << "WARNING in ObstacleMapScanner::scanLoop() Loop frequency is too high: " << -tDiff * 1000.0 << " milliseconds. Frequency: " << 1.0 / tElapsed << " Hz." << std::endl; }
    }
}

double ObstacleMapScanner::castRay( amor::types::Pose3& pose, amor::types::MultiLayerHeightField* mlhf, double angleOffset )
{
    double d = 0.0;
    double a = pose.yaw() + angleOffset;

    amor::types::Vector2 hdg( -sin( a ), cos( a ) );
    hdg *= 1000.0;

    amor::types::MultiLayerHeightField::CellCoord cc = mlhf->worldToGrid( pose.x(), pose.y() );

    if ( hdg.x() > 0 )
    {
        amor::types::Vector2 v1( cc.x(), cc.y() );

        amor::types::Vector2 v2 = amor::util::BresenhamLine::draw(
                cc.x(),
                cc.y(),
                cc.x() + static_cast<int>(hdg.x()),
                cc.y() + static_cast<int>(hdg.y()),
                &amor::modules::ObstacleMapScanner::checkCell,
                mlhf );

        d = ( (v2 - v1).length() * mlhf->resolution().x() ) + ( 1 * mlhf->resolution().x() ); // -> boxSize
    }
    else
    {
        amor::types::Vector2 v1( cc.x(), cc.y() );

        amor::types::Vector2 v2 = amor::util::BresenhamLine::drawReverse(
                cc.x(),
                cc.y(),
                cc.x() + static_cast<int>(hdg.x()),
                cc.y() + static_cast<int>(hdg.y()),
                &amor::modules::ObstacleMapScanner::checkCell,
                mlhf );

        d = ( (v2 - v1).length() * mlhf->resolution().x() ) + ( 1 * mlhf->resolution().x() ); // -> boxSize
    }

    return d;
}

bool ObstacleMapScanner::checkCell( int x, int y, void* userdata )
{
    amor::types::MultiLayerHeightField* mlhf = reinterpret_cast<amor::types::MultiLayerHeightField*>( userdata );

    int boxSize = 1;

    if ( ( x < (int)mlhf->size().x() - boxSize ) &&  ( y < (int)mlhf->size().y() - boxSize ) && ( x >= 0 + boxSize ) && ( y >= 0 + boxSize ) )
    {
        for ( int row = -boxSize; row<boxSize+1; row++ )
        {
            for ( int col = -boxSize; col<boxSize+1; col++ )
            {
                unsigned int idx = ( y + row ) * mlhf->size().x() + ( x + col );

                if ( ( mlhf->obstacleMap()->at( idx ) == amor::types::MultiLayerHeightField::OBST_BOUNDARY ) ||
                     ( mlhf->obstacleMap()->at( idx ) == amor::types::MultiLayerHeightField::OBST_UNKNOWN ) )
                {
                    return false;
                }
                else
                {
                    // Do nothing.
                }
            }
        }
    }
    else
    {
        return false;
    }

    return true;
}

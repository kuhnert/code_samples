#ifndef AMOR_MODULES_OBSTACLEMAPSCANNER_HPP_
#define AMOR_MODULES_OBSTACLEMAPSCANNER_HPP_

#include <iomanip>
#include <fstream>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>

#include <amor/types/Vector2.hpp>
#include <amor/types/Vector3.hpp>
#include <amor/types/Pose3.hpp>
#include <amor/types/PoseQ3.hpp>

#include <amor/clients/PathPlanningLocalClient/NodePath.h>
#include <amor/clients/ObstacleMapScannerClient/ObstacleMapScannerData.hpp>
#include <amor/clients/ObstacleMapScannerClient/ObstacleMapScannerCmd.hpp>

#include <amor/captain/module/AbstractEventInputWrapper.hpp>
#include <amor/captain/module/AbstractEventOutputWrapper.hpp>
#include <amor/captain/module/AbstractCommandInputWrapper.hpp>
#include <amor/captain/module/AbstractCommandOutputWrapper.hpp>
#include <amor/captain/module/UdpHeartbeatEmitter.hpp>
#include <amor/captain/module/exception/ModuleException.hpp>

#include <amor/util/BoostTime/BoostTime.hpp>
#include <amor/util/Bresenham/BresenhamLine.hpp>

#include <amor/packages/geometry/ScanlineProjector.hpp>

#include <io_wrappers/ObstacleMapScannerDIW.hpp>
#include <io_wrappers/ObstacleMapScannerDOW.hpp>

namespace amor {
namespace modules {

///\defgroup ObstacleMapScanner ObstacleMapScanner

/**
 * \brief Module ObstacleMapScanner.
 *
 * ObstacleMapScanner gets local obstacle grid maps and generates synthetic scanlines by casting rays from the estimated sensor position.
 * 
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 * \ingroup ObstacleMapScanner
 */
class ObstacleMapScanner
{
    public:

        /// Enumeration of possible module states.
        enum State
        {
            INVALID,    // Module has invalid state.
            IDLE,       // Module just created and is idling.
            INIT,       // Module is correctly initialized.
            START,      // Module started and calculating results.
            STOP,       // Module stopped but not resetted.
            RESET       // Module stopped and resetted.
        };

    private:
        
        /// Data input wrapper.
        ObstacleMapScannerDIW* diw_;
        
        /// Data output wrapper.
        ObstacleMapScannerDOW* dow_;
        
        /// Event input wrapper.
        amor::captain::module::AbstractEventInputWrapper* eiw_;
        
        /// Event output wrapper.
        amor::captain::module::AbstractEventOutputWrapper* eow_;
        
        /// Command input wrapper.
        amor::captain::module::AbstractCommandInputWrapper<ObstacleMapScannerCmd>* ciw_;
        
        /// Command output wrapper.
        amor::captain::module::AbstractCommandOutputWrapper<ObstacleMapScannerCmd>* cow_;

        /// Hint to enable/disable logging.
        bool log_;
        
        /// Current state of module.
        State state_;

        /// Heartbeat emitter of module.
        amor::captain::module::UdpHeartbeatEmitter heart_;

        /// Current obstacle map.
        amor::types::MultiLayerHeightField* map_;

        /// Current map reference pose.
        amor::types::PoseQ3 poseMap_;

        /// Size of vehicle bounding box.
        amor::types::Vector3 vehicleSize_;

        /// Minimal angle offset for scan synthesis.
        double minAngle_;

        /// Maximal angle offset for scan synthesis.
        double maxAngle_;

        /// Angle offset increment.
        double resAngle_;
                        
        /// Frequency with which the obstacle map scanning is performed [in Hz].
        double scanFrequency_;
        
        /// Period between two consecutive obstacle map scans [in seconds].
        double scanPeriod_;
        
        /// Thread keep-alive hint.
        bool runScan_;
        
        /// Thread for doing the obstacle map scanning loop.
        boost::thread* threadScanLoop_;
        
        /// Mutex for the obstacle map update.
        boost::mutex mtxMapUpdate_;
    
    public:

        /**
         * Constructor.
         * \param diw Data input wrapper.
         * \param dow Data output wrapper.
         * \param eiw Event input wrapper.
         * \param eow Event output wrapper.
         * \param ciw Command input wrapper.
         * \param cow Command output wrapper.
         * \param vehicleSize Size of vehicle bounding box.
         * \param minAngle Minimal angle offset for scan synthesis.
         * \param maxAngle Maximal angle offset for scan synthesis.
         * \param resAngle Angle offset increment.
         * \param scanFrequency Frequency with which the obstacle map scanning loop is operated [in Hz].
         * \param vis Hint to enable/disable visualization.
         * \param log Hint to enable/disable logging.
         */
        ObstacleMapScanner( ObstacleMapScannerDIW * diw,
                            ObstacleMapScannerDOW * dow,
                            amor::captain::module::AbstractEventInputWrapper * eiw,
                            amor::captain::module::AbstractEventOutputWrapper * eow,
                            amor::captain::module::AbstractCommandInputWrapper<ObstacleMapScannerCmd> * ciw,
                            amor::captain::module::AbstractCommandOutputWrapper<ObstacleMapScannerCmd> * cow,
                            std::string rootMissionXmlPath,
                            amor::types::Vector3 vehicleSize,
                            double minAngle,
                            double maxAngle,
                            double resAngle,
                            double scanFrequency = 100.0,
                            bool vis = false,
                            bool log = false );

        /**
         * Destructor.
         */
        virtual ~ObstacleMapScanner();

        /**
         * Initializes the module.
         */
        void init();
        
        /**
         * Starts the module.
         */
        void start();
        
        /**
         * Stops the module.
         */
        void stop();

        /**
         * Resets the module.
         */
        void reset();
        
    private:
        
        /**
         * Processes an incoming command.
         * \param cmd_ptr Smart pointer on command object.
         */
        void processCommand( boost::shared_ptr<ObstacleMapScannerCmd> cmd_ptr );

        /**
         * Send an event.
         * \param name Name string descriptor of event.
         * \param priority Priority setting of the event.
         * \param isAdressed Hint to define if the event is adressed.
         * \param adressee Group ID the event is adressed to (if adressed)
         */
        bool sendEvent( std::string name,
                        amor::captain::core::Event::PRIORITY priority = amor::captain::core::Event::NORMAL_PRIORITY,
                        bool isAddressed = false,
                        amor::captain::core::GROUP_ID addressee = 0 );

        /**
         * Processes a received obstacle map.
         * \param map Received obstacle map.
         * \param pose Pose associated with received map.
         */
        void processObstacleMap( amor::types::MultiLayerHeightField* map, amor::types::PoseQ3& pose );
                
        /**
         * Obstacle map scanning loop method.
         */
        void scanLoop();

        /**
         * Do raycasting to find the collision point with next obstacle in direction of angleOffset.
         * \param pose Current vehicle pose (relative from Odometry3D).
         * \param mlhf Obstacle map.
         * \param angleOffset Angular offset relative to looking direction of vehicle [in rad].
         * \return Distance to next obstacle in direction of angleOffset [in m].
         */
        double castRay( amor::types::Pose3& pose, amor::types::MultiLayerHeightField* mlhf, double angleOffset );

        /**
         * Callback used by bresenham line method to check if line growth should be continued or not.
         */
        static bool checkCell( int x, int y, void* userdata );
};

} // namespace modules
} // namespace amor

#endif /* AMOR_MODULES_OBSTACLEMAPSCANNER_HPP_ */

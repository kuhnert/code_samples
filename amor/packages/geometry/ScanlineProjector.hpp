#ifndef AMOR_PACKAGES_GEOMETRY_SCANLINEPROJECTOR_H_
#define AMOR_PACKAGES_GEOMETRY_SCANLINEPROJECTOR_H_

#include <amor/types/CartesianScanline.hpp>
#include <amor/types/PointCloud3.hpp>
#include <amor/util/Rotation/Rotation.hpp>

namespace amor {
namespace packages {
namespace geometry {

/**
 * \brief ScanlineProjector
 *
 * Class provides methods to project scanlines to a target coordinate frame based on a reference coordinate frame.
 *
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 */
template <typename VectorComponentType>
class ScanlineProjector
{
    public:

        // Convenience typedefs.
        typedef amor::types::TemplateVector3<VectorComponentType>                               Point;
        typedef amor::types::TemplatePointCloud3<VectorComponentType>                           PointCloud;
        typedef amor::types::TemplatePoseQ3<VectorComponentType,double>                         Pose;
        typedef amor::types::CartesianScanline<VectorComponentType,VectorComponentType,double>  Scanline;

    public:

        /**
         * Constructor.
         */
        ScanlineProjector() {}

        /**
         * Destructor.
         */
        virtual ~ScanlineProjector() {}

        /**
         * Projects a current pose into a reference coordiante frame defined by a reference pose.
         * \param currentPose Current pose.
         * \param referencePose Reference pose.
         * \return Projected pose.
         */
        Pose projectPose( Pose& currentPose, Pose& referencePose )
        {
            // Create copy of current pose for transformation.
            Pose transformedPose = currentPose;

            // Create BoostQuaternion from reference pose and invert it.
            BoostQuaternion qReferenceInverted = referencePose.orientation.toBoostQuaternion();
            BoostQuaternion qCurrent = currentPose.orientation.toBoostQuaternion();
            amor::util::Rotation::invert( qReferenceInverted );

            // Calculate the difference quaternion which realizes the difference rotation: current pose - reference pose.
            BoostQuaternion qDifference = qReferenceInverted * qCurrent;

            // Translation to local CS.
            transformedPose.position = transformedPose.position - referencePose.position;

            // Transform odometry measurement into local CS which has its origin defined by reference pose.
            amor::util::Rotation::rotate( qReferenceInverted, transformedPose.position );

            // Set difference quaternion for output.
            transformedPose.orientation = amor::types::TemplateQuaternion<double>( qDifference );

            return transformedPose;
        }

        /**
         * Projects a vector of points into a reference coordinate frame.
         * Sensor coordinate frame is transformed into the reference coordinate frame first.
         * Points are transformed into the reference coordinate frame defined by referencePose before adding them to the point cloud.
         * This is the version which writes the projected points directly back into the input vector overwriting the input points.
         * \param points Vector of input points.
         * \param currentPose Current pose.
         * \param referencePose Reference pose.
         * \param sensorOffsetPose Sensor offset pose.
         */
        void projectPoints( std::vector<Point>& points,
                            Pose& currentPose,
                            Pose& referencePose,
                            Pose& sensorOffsetPose )
        {
            // Project current pose. Reference pose defines the reference coordinate frame.
            Pose localPose = projectPose( currentPose, referencePose );

            // Build BoostQuartenions from input orientations for later calculations.
            BoostQuaternion qOffset = sensorOffsetPose.orientation.toBoostQuaternion();
            BoostQuaternion qDifference = localPose.orientation.toBoostQuaternion();

            // Transform all points into reference coordinate frame.
            for ( unsigned int i=0; i<points.size(); i++ )
            {
                // Apply sensor pose offset transformation.
                amor::util::Rotation::rotate( qOffset, points[i] );
                points[i] += sensorOffsetPose.position;

                // Apply difference pose transformation.
                amor::util::Rotation::rotate( qDifference, points[i] );
                points[i] += localPose.position;
            }
        }

        /**
         * Projects a vector of points into a reference coordinate frame.
         * Sensor coordinate frame is first transformed into the reference coordinate frame first.
         * Points are transformed into the reference coordinate frame defined by referencePose before adding them to the point cloud.
         * This is the version which writes the projected points into an output vector NOT overwriting the input points.
         * Additionally a boolean vector indicates which points are valid. Invalid points are not projected.
         * \param inPoints Vector of input points.
         * \param outPoints Vector of output points.
         * \param currentPose Current pose.
         * \param referencePose Reference pose.
         * \param sensorOffsetPose Sensor offset pose.
         * \param validPoints Point validity indicator vector.
         */
        void projectPoints( std::vector<Point>& inPoints,
                            std::vector<Point>& outPoints,
                            Pose& currentPose,
                            Pose& referencePose,
                            Pose& sensorOffsetPose,
                            std::vector<bool>* validPoints = 0 )
        {
            // Project current pose. Reference pose defines the reference coordinate frame.
            Pose localPose = projectPose( currentPose, referencePose );

            // Build BoostQuartenions from input orientations for later calculations.
            BoostQuaternion qOffset = sensorOffsetPose.orientation.toBoostQuaternion();
            BoostQuaternion qDifference = localPose.orientation.toBoostQuaternion();

            // Check whether we want to evaluate the valid boolean vector.
            bool doValidityCheck = ( validPoints != 0 );

            // Transform all points into reference coordinate frame.
            for ( unsigned int i=0; i<inPoints.size(); i++ )
            {
                // Skip invalid points.
                if ( doValidityCheck && !((*validPoints)[i]) ) { continue; }

                // Copy from input to output to avoid overwriting.
                outPoints[i] = inPoints[i];

                // Apply sensor pose offset transformation.
                amor::util::Rotation::rotate( qOffset, outPoints[i] );
                outPoints[i] += sensorOffsetPose.position;

                // Apply difference pose transformation.
                amor::util::Rotation::rotate( qDifference, outPoints[i] );
                outPoints[i] += localPose.position;
            }
        }

        /**
         * Add a scanline to a point cloud.
         * Sensor coordinate frame is first transformed into the reference coordinate frame.
         * Points are transformed into the reference coordinate frame defined by referencePose before adding them to the point cloud.
         * \param pointcloud Output point cloud.
         * \param scanline Input scanline.
         * \param referencePose Reference pose.
         * \param sensorOffsetPose Sensor offset pose.
         */
        void addCartesianScanline( PointCloud& pointcloud,
                                   Scanline& scanline,
                                   Pose& referencePose,
                                   Pose& sensorOffsetPose )
        {
            addPoints( pointcloud,
                       scanline.points,
                       scanline.poses.at(Scanline::IDX_POSE3_GYRO),
                       referencePose,
                       sensorOffsetPose );
        }

        /**
         * Add a vector of points to a point cloud.
         * Sensor coordinate frame is first transformed into the reference coordinate frame first.
         * Points are transformed into the reference coordinate frame defined by referencePose before adding them to the point cloud.
         * \param pointcloud Output point cloud.
         * \param points Input points.
         * \param currentPose Current pose.
         * \param referencePose Reference pose.
         * \param sensorOffsetPose Sensor offset pose.
         */
        void addPoints( PointCloud& pointcloud,
                        std::vector<Point>& points,
                        Pose& currentPose,
                        Pose& referencePose,
                        Pose& sensorOffsetPose )
        {
            // Project points to reference coordiante frame.
            projectPoints( points, currentPose, referencePose, sensorOffsetPose );

            for ( unsigned int i=0; i<points.size(); i++ ) { pointcloud.points.push_back( points[i] ); }
        }
};

} // namespace geometry
} // namespace packages
} // namespace amor

#endif /* AMOR_PACKAGES_GEOMETRY_SCANLINEPROJECTOR_H_ */


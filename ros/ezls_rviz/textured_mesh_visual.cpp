/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreTexture.h>
#include <OGRE/OgreMaterial.h>
#include <OGRE/OgreMaterialManager.h>
#include <OGRE/OgreTextureManager.h>
#include <OGRE/OgreManualObject.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <ezls_rviz/textured_mesh_visual.h>

namespace ezls_rviz
{

TexturedMeshVisual::TexturedMeshVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node, std::string topic, int mat_id, int tex_id, int obj_id ) :
    scene_node_( parent_node->createChildSceneNode() ),
    scene_manager_( scene_manager ),
    loaded_( false ),
    texture_loaded_( false ),
    alpha_( 0.0f ),
    points_only_( false ),
    x_offset_( 0.0f ),
    y_offset_( 0.0f ),
    z_offset_( 0.0f ),
    mat_id_( mat_id ),
    tex_id_( tex_id ),
    obj_id_( obj_id )
{
    // Initialize material properties. Disable lighting and set point size.
    std::stringstream ss;
    ss <<  "TexturedMeshObjectMaterial_" << topic << mat_id_;
    material_ = Ogre::MaterialManager::getSingleton().create( ss.str(), Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
    material_->setReceiveShadows( false );
    material_->getTechnique( 0 )->setLightingEnabled( false );
    material_->setLightingEnabled( false );
    material_->setPointSize( 3 );
}

TexturedMeshVisual::~TexturedMeshVisual()
{
    clear();
}

void TexturedMeshVisual::setMessage( const ezls_msgs::TexturedMesh::ConstPtr& msg )
{
    clear();

    if ( msg->use_texture)
    {
        texture_loaded_ = true;

        std::stringstream ss;
        ss << "MeshTexture_" << tex_id_;

        // Get texture image data from message.
        cv_bridge::CvImagePtr cv_ptr;
        try
        {
            cv_ptr = cv_bridge::toCvCopy( msg->texture, sensor_msgs::image_encodings::BGR8 );
        }
        catch ( cv_bridge::Exception& ex )
        {
            ROS_ERROR( "cv_bridge exception: %s", ex.what() );
            return;
        }

        int width = cv_ptr->image.cols;
        int height = cv_ptr->image.rows;

        unsigned int pixels_size =  width * height * 3;
        unsigned char* pixels = cv_ptr->image.data;

        Ogre::DataStreamPtr pixel_stream;
        pixel_stream.bind( new Ogre::MemoryDataStream( pixels, pixels_size ) );

        // Create texture, fill it with data from incoming message and set texture properties.
        texture_ = Ogre::TextureManager::getSingleton().loadRawData( ss.str(),
                                                                     Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                                     pixel_stream,
                                                                     width,
                                                                     height,
                                                                     Ogre::PF_R8G8B8,
                                                                     Ogre::TEX_TYPE_2D,
                                                                     0);

        Ogre::Pass* pass = material_->getTechnique( 0 )->getPass( 0 );

        Ogre::TextureUnitState* tex_unit = 0;

        if ( pass->getNumTextureUnitStates() > 0 )
        {
            tex_unit = pass->getTextureUnitState( 0 );
        }
        else
        {
            tex_unit = pass->createTextureUnitState();
        }

        tex_unit->setTextureName( texture_->getName() );
        tex_unit->setTextureFiltering( Ogre::TFO_NONE );
    }

    if ( msg->points.size() > 0 )
    {
        std::stringstream ss2;
        ss2 << "MeshObject_" << obj_id_;
        manual_object_ = scene_manager_->createManualObject( ss2.str() );

        // Set rendering operation. Displays a triangle mesh or a point cloud.
        if ( points_only_ )
            manual_object_->begin( material_->getName(), Ogre::RenderOperation::OT_POINT_LIST );
        else
            manual_object_->begin( material_->getName(), Ogre::RenderOperation::OT_TRIANGLE_LIST );

        // Set points, normals, texture coordinates and colours (if texture is not used).
        for ( size_t i = 0; i < msg->points.size(); i++ )
        {
            manual_object_->position( msg->points[i].x + x_offset_, msg->points[i].y + y_offset_, msg->points[i].z + z_offset_ );

            if ( i < msg->normal.size() )
                manual_object_->normal( msg->normal[i].x, msg->normal[i].y, msg->normal[i].z );
            else if ( msg->normal.size() > 0 )
                manual_object_->normal( msg->normal[msg->normal.size()-1].x, msg->normal[msg->normal.size()-1].y, msg->normal[msg->normal.size()-1].z );

            if ( msg->use_texture )
            {
                if ( i < msg->texture_coord.size() )
                    manual_object_->textureCoord( msg->texture_coord[i].x, msg->texture_coord[i].y );
                else if ( msg->texture_coord.size() > 0 )
                    manual_object_->textureCoord( msg->texture_coord[msg->texture_coord.size()-1].x, msg->texture_coord[msg->texture_coord.size()-1].y );
            }
            else
            {
                if ( i < msg->color.size() )
                    manual_object_->colour( msg->color[i].x, msg->color[i].y, msg->color[i].z, alpha_ );
                else if ( msg->color.size() > 0 )
                    manual_object_->colour( msg->color[msg->color.size()-1].x, msg->color[msg->color.size()-1].y, msg->color[msg->color.size()-1].z, alpha_ );
            }
        }

        // Set triangle vertex indices if we want to display triangles.
        if ( !points_only_ )
        {
            for ( size_t i = 0; i < msg->triangle_list.size(); i++ )
            {
                int p1 = static_cast<int>( msg->triangle_list[i].x );
                int p2 = static_cast<int>( msg->triangle_list[i].y );
                int p3 = static_cast<int>( msg->triangle_list[i].z );

                manual_object_->triangle( p1, p2, p3 );
            }
        }

        manual_object_->end();

        scene_node_->attachObject( manual_object_ );

        loaded_ = true;
    }
    else
    {
        // Do nothing.
    }

    // Make a copy of the current mesh for redrawing purposes after changing properties.
    old_mesh_ = msg;
}

void TexturedMeshVisual::setFramePosition( const Ogre::Vector3& position )
{
    scene_node_->setPosition( position );
}

void TexturedMeshVisual::setFrameOrientation( const Ogre::Quaternion& orientation )
{
    scene_node_->setOrientation( orientation );
}

void TexturedMeshVisual::setAlpha( float alpha )
{
    alpha_ = alpha;

    Ogre::Pass* pass = material_->getTechnique( 0 )->getPass( 0 );

    Ogre::TextureUnitState* tex_unit = 0;

    if ( pass->getNumTextureUnitStates() > 0 )
        tex_unit = pass->getTextureUnitState( 0 );
    else
        tex_unit = pass->createTextureUnitState();

    tex_unit->setAlphaOperation( Ogre::LBX_SOURCE1, Ogre::LBS_MANUAL, Ogre::LBS_CURRENT, alpha_ );

    if ( alpha_ < 0.9998 )
        material_->setSceneBlending( Ogre::SBT_TRANSPARENT_ALPHA );
    else
        material_->setSceneBlending( Ogre::SBT_REPLACE );

    if ( loaded_ )
        setMessage( old_mesh_ );
}

void TexturedMeshVisual::setXOffset( float x_offset )
{
    x_offset_ = x_offset;

    if ( loaded_ )
        setMessage( old_mesh_ );
}

void TexturedMeshVisual::setYOffset( float y_offset )
{
    y_offset_ = y_offset;

    if ( loaded_ )
        setMessage( old_mesh_ );
}

void TexturedMeshVisual::setZOffset( float z_offset )
{
    z_offset_ = z_offset;

    if ( loaded_ )
        setMessage( old_mesh_ );
}

void TexturedMeshVisual::setPointsOnly( bool points_only )
{
    points_only_ = points_only;

    if ( loaded_ )
        setMessage( old_mesh_ );
}

void TexturedMeshVisual::clear()
{
    if ( !loaded_ )
        return;

    // Destroy the frame node since we don't need it anymore.
    scene_manager_->destroyManualObject( manual_object_ );
    manual_object_ = 0;

    // Reset texture.
    if ( texture_loaded_ )
    {
        std::string tex_name = texture_->getName();
        texture_.setNull();
        Ogre::TextureManager::getSingleton().unload( tex_name );
    }

    texture_loaded_ = false;
    loaded_ = false;
}

}

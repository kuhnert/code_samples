/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/properties/property.h>
#include <rviz/properties/property_manager.h>
#include <rviz/frame_manager.h>

#include <ezls_rviz/textured_mesh_visual.h>
#include <ezls_rviz/textured_mesh_display.h>

namespace ezls_rviz
{

TexturedMeshDisplay::TexturedMeshDisplay() :
    Display(),
    scene_node_( 0 ),
    messages_received_( 0 ),
    topic_( "" ),
    alpha_( 1.0f ),
    points_only_( false ),
    x_offset_( 0.0f ),
    y_offset_( 0.0f ),
    z_offset_( 0.0f ),
    mat_counter_( 1 ),
    tex_counter_( 1 ),
    obj_counter_( 1 )
{
}

void TexturedMeshDisplay::onInitialize()
{
    // Make an Ogre::SceneNode to contain all our visuals.
    scene_node_ = scene_manager_->getRootSceneNode()->createChildSceneNode();

    // Set the default history length and resize the visuals_ array.
    setHistoryLength( 1 );

    // A tf::MessageFilter listens to ROS messages and calls our callback with them when they can be matched up with valid tf transform data.
    tf_filter_ = new tf::MessageFilter<ezls_msgs::TexturedMesh>( *vis_manager_->getTFClient(), "", 100, update_nh_ );
    tf_filter_->connectInput( sub_ );
    tf_filter_->registerCallback( boost::bind( &TexturedMeshDisplay::incomingMessage, this, _1 ) );

    // FrameManager has some built-in functions to set the status of a Display based on callbacks from a tf::MessageFilter.
    // These work fine for this simple display.
    vis_manager_->getFrameManager()->registerFilterForTransformStatusCheck( tf_filter_, this );
}

TexturedMeshDisplay::~TexturedMeshDisplay()
{
    unsubscribe();

    clear();

    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        delete visuals_[ i ];
    }

    delete tf_filter_;
}

void TexturedMeshDisplay::clear()
{
    // Clear the visuals by deleting their objects.
    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        delete visuals_[ i ];
        visuals_[ i ] = 0;
    }

    tf_filter_->clear();

    messages_received_ = 0;

    setStatus( rviz::status_levels::Warn, "Topic", "No messages received" );
}

void TexturedMeshDisplay::setTopic( const std::string& topic )
{
    unsubscribe();
    clear();
    topic_ = topic;
    subscribe();

    propertyChanged( topic_property_ );

    causeRender();
}

void TexturedMeshDisplay::setAlpha( float alpha )
{
    alpha_ = alpha;
    propertyChanged( alpha_property_ );

    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        if( visuals_[ i ] )
        {
            visuals_[ i ]->setAlpha( alpha_ );
        }
    }

    causeRender();
}

void TexturedMeshDisplay::setXOffset( double x_offset )
{
    x_offset_ = x_offset;
    propertyChanged( x_offset_property_ );

    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        if( visuals_[ i ] )
        {
            visuals_[ i ]->setXOffset( x_offset_  );
        }
    }

    causeRender();
}

void TexturedMeshDisplay::setYOffset( double y_offset )
{
    y_offset_ = y_offset;
    propertyChanged( y_offset_property_ );

    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        if( visuals_[ i ] )
        {
            visuals_[ i ]->setYOffset( y_offset_  );
        }
    }

    causeRender();
}

void TexturedMeshDisplay::setZOffset( double z_offset )
{
    z_offset_ = z_offset;
    propertyChanged( z_offset_property_ );

    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        if( visuals_[ i ] )
        {
            visuals_[ i ]->setZOffset( z_offset_  );
        }
    }

    causeRender();
}

void TexturedMeshDisplay::setHistoryLength( int length )
{
    // Don't let people enter invalid values.
    if( length < 1 )
        length = 1;

    // If the length is not changing, we don't need to do anything.
    if( history_length_ == length )    
        return;

    // Set the actual variable.
    history_length_ = length;
    propertyChanged( history_length_property_ );

    // Create a new array of visual pointers, all 0.
    std::vector<TexturedMeshVisual*> new_visuals( history_length_, (TexturedMeshVisual*)0 );

    // Copy the contents from the old array to the new. (Number to copy is the minimum of the 2 vector lengths).
    size_t copy_len = (new_visuals.size() > visuals_.size()) ? visuals_.size() : new_visuals.size();
    for( size_t i = 0; i < copy_len; i++ )
    {
        int new_index = ( messages_received_ - i ) % new_visuals.size();
        int old_index = ( messages_received_ - i ) % visuals_.size();
        new_visuals[ new_index ] = visuals_[ old_index ];
        visuals_[ old_index ] = 0;
    }

    // Delete any remaining old visuals
    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        delete visuals_[ i ];
    }

    // We don't need to create any new visuals here, they are created as needed when messages are received.

    // Put the new vector into the member variable version and let the old one go out of scope.
    visuals_.swap( new_visuals );
}

void TexturedMeshDisplay::setPointsOnly( bool points_only )
{
    points_only_ = points_only;
    propertyChanged( points_only_property_ );

    for( size_t i = 0; i < visuals_.size(); i++ )
    {
        if( visuals_[ i ] )
        {
            visuals_[ i ]->setPointsOnly( points_only_ );
        }
    }

    causeRender();
}

void TexturedMeshDisplay::subscribe()
{
    // If we are not actually enabled, don't do it.
    if ( !isEnabled() )
        return;

    // Try to subscribe to the current topic name.
    try
    {
        sub_.subscribe( update_nh_, topic_, 10 );
        setStatus( rviz::status_levels::Ok, "Topic", "OK" );
    }
    catch( ros::Exception& e )
    {
        setStatus( rviz::status_levels::Error, "Topic", std::string( "Error subscribing: " ) + e.what() );
    }
}

void TexturedMeshDisplay::unsubscribe()
{
    sub_.unsubscribe();
}

void TexturedMeshDisplay::onEnable()
{
    subscribe();
}

void TexturedMeshDisplay::onDisable()
{
    unsubscribe();
    clear();
}

void TexturedMeshDisplay::fixedFrameChanged()
{
    tf_filter_->setTargetFrame( fixed_frame_ );
    clear();
}

void TexturedMeshDisplay::incomingMessage( const ezls_msgs::TexturedMesh::ConstPtr& msg )
{
    ++messages_received_;

    // Each display can have multiple status lines.
    // This one is called "Topic" and says how many messages have been received in this case.
    std::stringstream ss;
    ss << messages_received_ << " messages received";
    setStatus( rviz::status_levels::Ok, "Topic", ss.str() );

    // Here we call the rviz::FrameManager to get the transform from the fixed frame to the frame in the header of this TexturedMesh message.
    // If it fails, we can't do anything else so we return.
    Ogre::Quaternion orientation;
    Ogre::Vector3 position;
    if( !vis_manager_->getFrameManager()->getTransform( msg->header.frame_id,
                                                        msg->header.stamp,
                                                        position, orientation ) )
    {
        ROS_DEBUG( "Error transforming from frame '%s' to frame '%s'", msg->header.frame_id.c_str(), fixed_frame_.c_str() );
        return;
    }

    // We are keeping a circular buffer of visual pointers.
    // This gets the next one, or creates and stores it if it was missing.
    TexturedMeshVisual* visual = visuals_[ messages_received_ % history_length_ ];

    if( visual == 0 )
    {
        visual = new TexturedMeshVisual( vis_manager_->getSceneManager(),
                                         scene_node_,
                                         topic_,
                                         mat_counter_++,
                                         tex_counter_++,
                                         obj_counter_++ );
        visuals_[ messages_received_ % history_length_ ] = visual;
    }

    // Now set or update the contents of the chosen visual.
    visual->setMessage( msg );
    visual->setFramePosition( position );
    visual->setFrameOrientation( orientation );
    visual->setAlpha( alpha_ );
    visual->setPointsOnly( points_only_ );
}

void TexturedMeshDisplay::reset()
{
    Display::reset();
    clear();
}

void TexturedMeshDisplay::createProperties()
{
    topic_property_ = property_manager_->createProperty<rviz::ROSTopicStringProperty>( "Topic",
                                                                                       property_prefix_,
                                                                                       boost::bind( &TexturedMeshDisplay::getTopic, this ),
                                                                                       boost::bind( &TexturedMeshDisplay::setTopic, this, _1 ),
                                                                                       parent_category_,
                                                                                       this );
    setPropertyHelpText( topic_property_, "Topic to subscribe to." );
    rviz::ROSTopicStringPropertyPtr topic_prop = topic_property_.lock();
    topic_prop->setMessageType( ros::message_traits::datatype<ezls_msgs::TexturedMesh>() );

    alpha_property_ = property_manager_->createProperty<rviz::FloatProperty>( "Alpha",
                                                                              property_prefix_,
                                                                              boost::bind( &TexturedMeshDisplay::getAlpha, this ),
                                                                              boost::bind( &TexturedMeshDisplay::setAlpha, this, _1 ),
                                                                              parent_category_,
                                                                              this );
    setPropertyHelpText( alpha_property_, "0.0 is fully transparent, 1.0 is fully opaque." );

    history_length_property_ = property_manager_->createProperty<rviz::IntProperty>( "History Length",
                                                                                     property_prefix_,
                                                                                     boost::bind( &TexturedMeshDisplay::getHistoryLength, this ),
                                                                                     boost::bind( &TexturedMeshDisplay::setHistoryLength, this, _1 ),
                                                                                     parent_category_,
                                                                                     this );
    setPropertyHelpText( history_length_property_, "Number of prior measurements to display." );

    points_only_property_ = property_manager_->createProperty<rviz::BoolProperty>( "Points only",
                                                                                   property_prefix_,
                                                                                   boost::bind( &TexturedMeshDisplay::getPointsOnly, this ),
                                                                                   boost::bind( &TexturedMeshDisplay::setPointsOnly, this, _1 ),
                                                                                   parent_category_,
                                                                                   this );
    setPropertyHelpText( points_only_property_, "Only draw points, no triangles are drawn." );

    x_offset_property_ = property_manager_->createProperty<rviz::FloatProperty>( "X Offset",
                                                                                 property_prefix_,
                                                                                 boost::bind( &TexturedMeshDisplay::getXOffset, this ),
                                                                                 boost::bind( &TexturedMeshDisplay::setXOffset, this, _1 ),
                                                                                 parent_category_,
                                                                                 this );
    setPropertyHelpText( x_offset_property_, "X Offset for mesh position." );

    y_offset_property_ = property_manager_->createProperty<rviz::FloatProperty>( "Y Offset",
                                                                                 property_prefix_,
                                                                                 boost::bind( &TexturedMeshDisplay::getYOffset, this ),
                                                                                 boost::bind( &TexturedMeshDisplay::setYOffset, this, _1 ),
                                                                                 parent_category_,
                                                                                 this );
    setPropertyHelpText( y_offset_property_, "Y Offset for mesh position." );

    z_offset_property_ = property_manager_->createProperty<rviz::FloatProperty>( "Z Offset",
                                                                                 property_prefix_,
                                                                                 boost::bind( &TexturedMeshDisplay::getZOffset, this ),
                                                                                 boost::bind( &TexturedMeshDisplay::setZOffset, this, _1 ),
                                                                                 parent_category_,
                                                                                 this );
    setPropertyHelpText( z_offset_property_, "Z Offset for mesh position." );
}

}

// Tell pluginlib about this class.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( ezls_rviz, TexturedMesh, ezls_rviz::TexturedMeshDisplay, rviz::Display )

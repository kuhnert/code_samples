/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TEXTURED_MESH_DISPLAY_H
#define TEXTURED_MESH_DISPLAY_H

#include <rviz/display.h>
#include <message_filters/subscriber.h>
#include <tf/message_filter.h>
#include <ezls_msgs/TexturedMesh.h>

namespace Ogre
{
class SceneNode;
}

namespace ezls_rviz
{

class TexturedMeshVisual;

/**
 * @brief TexturedMeshDisplay
 *
 * The TexturedMeshDisplay class is used to define a new display type for RVIZ.
 * It can be used to display a triangle mesh including texture image for example
 * recorded by a laser scanner and a registered camera.
 *
 * The TexturedMeshDisplay class itself just implements a circular buffer,
 * editable parameters, and Display subclass machinery.  The visuals
 * themselves are represented by a separate class, TexturedMeshVisual.
 * The idiom for the visuals is that when the objects exist, they appear
 * in the scene, and when they are deleted, they disappear.
 *
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 */
class TexturedMeshDisplay: public rviz::Display
{

public:

    /**
     * @brief Constructor.
     */
    TexturedMeshDisplay();

    /**
     * @brief Destructor.
     */
    virtual ~TexturedMeshDisplay();

    /**
     * @brief Overrides public virtual function onInitialize from Display class.
     */
    virtual void onInitialize();

    /**
     * @brief Overrides public virtual function fixedFrameChanged from Display class.
     */
    virtual void fixedFrameChanged();

    /**
     * @brief Overrides public virtual function reset from Display class.
     */
    virtual void reset();

    /**
     * @brief Overrides public virtual function createProperties from Display class.
     */
    virtual void createProperties();

    /**
     * @brief Sets user-editable property: topic.
     * @param topic Topic descriptor.
     */
    void setTopic(const std::string& topic);

    /**
     * @brief Gets user-editable property: topic.
     * @return Topic descriptor.
     */
    const std::string& getTopic() { return topic_; }

    /**
     * @brief Sets user-editable property: alpha.
     * @param alpha Alpha value.
     */
    void setAlpha( float alpha );

    /**
     * @brief Gets user-editable property: alpha.
     * @return Alpha value.
     */
    float getAlpha() { return alpha_; }

    /**
     * @brief Sets user-editable property: history length.
     * @param history_length History length.
     */
    void setHistoryLength( int history_length );

    /**
     * @brief Gets user-editable property: history length.
     * @return History length.
     */
    int getHistoryLength() const { return history_length_; }

    /**
     * @brief Sets user-editable property: display points only.
     * @param points_only Display points only hint.
     */
    void setPointsOnly( bool points_only );

    /**
     * @brief Gets user-editable property: display points only.
     * @return Display points only hint.
     */
    int getPointsOnly() const { return points_only_; }

    /**
     * @brief Sets user-editable property: x offset.
     * @param x_offset X offset.
     */
    void setXOffset( double x_offset );

    /**
     * @brief Gets user-editable property: x offset.
     * @return X offset.
     */
    double getXOffset() const { return x_offset_; }

    /**
     * @brief Sets user-editable property: y offset.
     * @param y_offset Y offset.
     */
    void setYOffset( double y_offset );

    /**
     * @brief Gets user-editable property: y offset.
     * @return Y offset.
     */
    double getYOffset() const { return y_offset_; }

    /**
     * @brief Sets user-editable property: z offset.
     * @param z_offset Z offset.
     */
    void setZOffset( double z_offset );

    /**
     * @brief Gets user-editable property: z offset.
     * @return Z offset.
     */
    double getZOffset() const { return z_offset_; }

protected:

    /**
     * @brief Overrides protected virtual function onEnable from Display class.
     */
    virtual void onEnable();

    /**
     * @brief Overrides protected virtual function onDisable from Display class.
     */
    virtual void onDisable();

private:

    /**
     * @brief Handles an incoming ROS messages.
     * @param msg Incoming message.
     */
    void incomingMessage( const ezls_msgs::TexturedMesh::ConstPtr& msg );

    /**
     * @brief Subscribes the subscriber sub_ to topic_.
     */
    void subscribe();

    /**
     * @brief Unsubscribes the subscriber sub_ from topic_.
     */
    void unsubscribe();

    /**
     * @brief Clears display back to initial state.
     */
    void clear();

    /// Storage for the list of visuals.
    std::vector<TexturedMeshVisual*> visuals_;

    /// Node in the Ogre scene tree to be the parent of all our visuals.
    Ogre::SceneNode* scene_node_;

    /// Filtered ROS subscriber.
    message_filters::Subscriber<ezls_msgs::TexturedMesh> sub_;

    /// TF message filter.
    tf::MessageFilter<ezls_msgs::TexturedMesh>* tf_filter_;

    /// Number of received messages.
    int messages_received_;

    /// Topic descriptor (user-editable property variable).
    std::string topic_;

    /// Alpha value (user-editable property variable).
    float alpha_;

    /// History length (user-editable property variable).
    int history_length_;

    /// Points only display hint (user-editable property variable).
    bool points_only_;

    /// X offset (user-editable property variable).
    float x_offset_;

    /// Y offset (user-editable property variable).
    float y_offset_;

    /// Z offset (user-editable property variable).
    float z_offset_;

    /// Property objects for topic_ property.
    rviz::ROSTopicStringPropertyWPtr topic_property_;

    /// Property objects for alpha_ property.
    rviz::FloatPropertyWPtr alpha_property_;

    /// Property objects for history_length_ property.
    rviz::IntPropertyWPtr history_length_property_;

    /// Property objects for points_only_ property.
    rviz::BoolPropertyWPtr points_only_property_;

    /// Property objects for x_offset_ property.
    rviz::FloatPropertyWPtr x_offset_property_;

    /// Property objects for y_offset_ property.
    rviz::FloatPropertyWPtr y_offset_property_;

    /// Property objects for z_offset_ property.
    rviz::FloatPropertyWPtr z_offset_property_;

    /// Material ID counter for visual identification.
    int mat_counter_;

    /// Texture ID counter for visual identification.
    int tex_counter_;

    /// Object ID counter for visual identification.
    int obj_counter_;
};

}

#endif

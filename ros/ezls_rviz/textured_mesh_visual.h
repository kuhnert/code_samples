/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TEXTURED_MESH_VISUAL_H
#define TEXTURED_MESH_VISUAL_H

#include <ezls_msgs/TexturedMesh.h>

namespace Ogre
{
class Vector3;
class Quaternion;
}

namespace ezls_rviz
{

/**
 * \brief TexturedMeshVisual
 *
 * The TexturedMeshVisual is used with the class TexturedMeshDisplay to display a textured triangle mesh in ROS.
 * It can be used to display a triangle mesh including texture image for example recorded by a laser scanner and
 * a registered camera.
 *
 * The TexturedMeshVisual deals with all the visual stuff including setting and updating points, normals, colours,
 * triangle indices, texture coordinates and texture images.
 *
 * \author Lars Kuhnert (lars.kuhnert@fb12.uni-siegen.de)
 */
class TexturedMeshVisual
{

public:

    /**
     * @brief Constructor.
     * @param scene_manager Ogre scene manager.
     * @param parent_node Ogre parent node.
     * @param topic Topic descriptor associated with this visual.
     * @param mat_id Material ID associated with this visual.
     * @param tex_id Texture ID associated with this visual.
     * @param obj_id Object ID associated with this visual.
     */
    TexturedMeshVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node, std::string topic, int mat_id, int tex_id, int obj_id );

    /**
     * @brief Destructor.
     * Removes the visual stuff from the scene.
     */
    virtual ~TexturedMeshVisual();

    /**
     * @brief Sets the message which is displayed in this visual.
     * @param msg Message which is displayed in this visual.
     */
    void setMessage( const ezls_msgs::TexturedMesh::ConstPtr& msg );

    /**
     * @brief Set the position of the coordinate frame the message refers to.
     * @param position Position of the coordinate frame the message refers to.
     */
    void setFramePosition( const Ogre::Vector3& position );

    /**
     * @brief Set the orientation of the coordinate frame the message refers to.
     * @param orientation Orientation of the coordinate frame the message refers to.
     */
    void setFrameOrientation( const Ogre::Quaternion& orientation );

    /**
     * @brief Sets property of visual: alpha.
     * @param alpha Alpha value of visual.
     */
    void setAlpha( float alpha );

    /**
     * @brief Sets property of visual: display points only.
     * @param points_only Display points only hint of visual.
     */
    void setPointsOnly( bool points_only );

    /**
     * @brief Sets property of visual: X offset.
     * @param x_offset X offset of visual.
     */
    void setXOffset( float x_offset );

    /**
     * @brief Sets property of visual: Y offset.
     * @param y_offset Y offset of visual.
     */
    void setYOffset( float y_offset );

    /**
     * @brief Sets property of visual: z offset.
     * @param z_offset Z offset of visual.
     */
    void setZOffset( float z_offset );

protected:

    /**
     * @brief Clears frame node and resets texture.
     */
    void clear();

private:

    /// Ogre scene node.
    Ogre::SceneNode* scene_node_;

    /// Ogre scene manager.
    Ogre::SceneManager* scene_manager_;

    /// Ogre manual object storing the textured mesh.
    Ogre::ManualObject* manual_object_;

    /// Texture used for texturing the mesh.
    Ogre::TexturePtr texture_;

    /// Material used for texturing the mesh.
    Ogre::MaterialPtr material_;

    /// Hint indicating that a valid mesh has been loaded.
    bool loaded_;

    /// Hint indicating that a valid texture has been loaded.
    bool texture_loaded_;

    /// Property of visual: Alpha value.
    float alpha_;

    /// Property of visual: Points only display hint.
    bool points_only_;

    /// Property of visual: X offset.
    float x_offset_;

    /// Property of visual: Y offset.
    float y_offset_;

    /// Property of visual: Z offset.
    float z_offset_;

    /// Material ID associated with this visual.
    int mat_id_;

    /// Texture ID associated with this visual.
    int tex_id_;

    /// Object ID associated with this visual.
    int obj_id_;

    /// Copy of the last received mesh for redrawing purposes after changing properties.
    ezls_msgs::TexturedMesh::ConstPtr old_mesh_;
};

}

#endif

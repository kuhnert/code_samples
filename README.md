# C++ code samples of Lars Kuhnert.

# /amor/modules/ObstacleMapScanner/*

Exemplary software module for doing ray-casting on obstacle maps to create synthetic laser scans. Abstract input-/output wrappers for data, events and commands allow an easy exchange of communication mechanisms and prevent the intermixture of complex data processing algorithms and administrative software components.

# /amor/packages/geometry/*

Templatized class for projection of poses and geometric points (e.g. originating from laser scans). Transformations are based on quaternions for performance reasons.

# /amor/captain/module/*

Heartbeat monitor and emitter based on UDP sockets which is able to monitor the health status of software modules while a robot is acting autonomously to prevent unexpected robot behaviour on failure of individual software components.

# /ros/ezls_rviz/*

RViz display type plugin which allows to visualize textured triangle meshes (e.g. originating from laser-scanned, triangulated point clouds with texture images of registered cameras) in contrast to simple point clouds.

